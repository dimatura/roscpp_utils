#include "roscpp_utils/marker_util.h"

namespace roscpp_utils
{

MarkerWrapper MarkerWrapper::create_upside_down_pyramids(float w, float h,
                                                         const std::vector<Eigen::Vector3f>& locations) {

  using Vec3f = Eigen::Vector3f;

  MarkerWrapper mw;
  mw.set_type_triangle_list();
  mw.set_scale(1., 1., 1.);
  mw.set_rgb_int(0xff, 0xa5, 0x00);

  Vec3f top_left_far(-w/2.f, w/2.f, h);
  Vec3f top_right_far(w/2.f, w/2.f, h);

  Vec3f top_left_near(-w/2.f, -w/2.f, h);
  Vec3f top_right_near(w/2.f, -w/2.f, h);

  std_msgs::ColorRGBA color1 = ColorWrapper::create_from_int(0xff, 0xa5, 0x00).get_msg();
  // darker
  std_msgs::ColorRGBA color2 = ColorWrapper::create_from_int(0xd4, 0x92, 0x0f).get_msg();
  // lighter
  std_msgs::ColorRGBA color3 = ColorWrapper::create_from_int(0xfd, 0xc3, 0x4d).get_msg();

  for (const Vec3f& loc : locations) {
    // side
    mw.add_point(loc);
    mw.add_point(Vec3f(loc+top_left_near));
    mw.add_point(Vec3f(loc+top_right_near));
    mw.add_color(color1);

    // side
    mw.add_point(loc);
    mw.add_point(Vec3f(loc+top_left_near));
    mw.add_point(Vec3f(loc+top_left_far));
    mw.add_color(color2);

    // side
    mw.add_point(loc);
    mw.add_point(Vec3f(loc+top_left_far));
    mw.add_point(Vec3f(loc+top_right_far));
    mw.add_color(color1);

    // side
    mw.add_point(loc);
    mw.add_point(Vec3f(loc+top_right_near));
    mw.add_point(Vec3f(loc+top_right_far));
    mw.add_color(color2);

    // top 1
    mw.add_point(Vec3f(loc+top_left_near));
    mw.add_point(Vec3f(loc+top_left_far));
    mw.add_point(Vec3f(loc+top_right_far));
    mw.add_color(color3);

    // top 2
    mw.add_point(Vec3f(loc+top_right_far));
    mw.add_point(Vec3f(loc+top_right_near));
    mw.add_point(Vec3f(loc+top_left_near));
    mw.add_color(color3);
  }
  return mw;
}

MarkerWrapper MarkerWrapper::create_frustum2d(const Eigen::Isometry3f& M,
                                              float fx,
                                              float cx,
                                              int width,
                                              float max_z) {

  using Vec3f = Eigen::Vector3f;
  using Isometry3f = Eigen::Isometry3f;

  Vec3f left_corner((0-cx)*max_z/fx, 0, max_z);
  Vec3f right_corner((width-cx)*max_z/fx, 0, max_z);
  Isometry3f Mf = M.cast<float>();
  Vec3f origin = Mf.translation();
  left_corner = Mf*left_corner;
  right_corner = Mf*right_corner;

  MarkerWrapper mw;
  mw.set_type_line_strip();
  mw.set_scale(1., 0., 0.);
  mw.set_rgb_int(0xff, 0xa5, 0x00).set_alpha(1.0);
  mw.set_lifetime(ros::Duration());
  mw.add_point(origin);
  mw.add_point(left_corner);
  mw.add_point(right_corner);
  mw.add_point(origin);
  return mw;
}

MarkerWrapper MarkerWrapper::create_frustum3d(const Eigen::Isometry3f& M,
                                              float fx, float fy,
                                              float cx, float cy,
                                              int width, int height,
                                              float max_z) {

  using Vec3f = Eigen::Vector3f;
  using Isometry3f = Eigen::Isometry3f;

  // TODO standardize
  Isometry3f Mf = M.cast<float>();

  // get camera triangle (frustum projected on ground)
  Vec3f top_left_corner((0-cx)*max_z/fx, (0-cy)*max_z/fy, max_z);
  Vec3f top_right_corner((width-cx)*max_z/fx, (0-cy)*max_z/fy, max_z);

  Vec3f bottom_left_corner((0-cx)*max_z/fy, (height-cy)*max_z/fy, max_z);
  Vec3f bottom_right_corner((width-cx)*max_z/fy, (height-cy)*max_z/fy, max_z);

  Vec3f origin = Mf.translation();
  top_left_corner = Mf*top_left_corner;
  top_right_corner = Mf*top_right_corner;
  bottom_left_corner = Mf*bottom_left_corner;
  bottom_right_corner = Mf*bottom_right_corner;

  MarkerWrapper mw;
  mw.set_type_line_list();
  constexpr float lw = 0.4;
  mw.set_scale(lw, 0., 0.);
  mw.set_rgb_int(0xff, 0x00, 0x00);

  mw.add_point(origin);
  mw.add_point(top_left_corner);

  mw.add_point(top_left_corner);
  mw.add_point(top_right_corner);

  mw.add_point(top_right_corner);
  mw.add_point(origin);

  mw.add_point(origin);
  mw.add_point(bottom_left_corner);

  mw.add_point(bottom_left_corner);
  mw.add_point(bottom_right_corner);

  mw.add_point(bottom_right_corner);
  mw.add_point(origin);

  mw.add_point(top_left_corner);
  mw.add_point(bottom_left_corner);

  mw.add_point(top_right_corner);
  mw.add_point(bottom_right_corner);

  return mw;
}

} /* roscpp_utils { */
