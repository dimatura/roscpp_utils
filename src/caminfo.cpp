#include "roscpp_utils/caminfo.h"

namespace roscpp_utils
{

void CamInfo::init(float fx,
                   float fy,
                   float cx,
                   float cy,
                   float Tx,
                   int width,
                   int height) {
  this->fx = fx;
  this->fy = fy;
  this->cx = cx;
  this->cy = cy;
  this->Tx = Tx;
  this->width = width;
  this->height = height;
  initialized_ = true;
}

void CamInfo::init_from_msg(const sm::CameraInfo& caminfo) {
  fx = caminfo.P[0];
  fy = caminfo.P[5];
  cx = caminfo.P[2];
  cy = caminfo.P[6];
  Tx = caminfo.P[3];
  width = caminfo.width;
  height = caminfo.height;
  initialized_ = true;
}

/**
 * TODO.
 * Note that we are not scaling Tx, even though it would make sense,
 * given that Tx = -b*f, i.e. it depends on focal length, which is scaled.
 * However we are currently keeping disparities as-is when scaling images (see
 * disp_proc), as they are in (sub)pixel units -- so if we keep Tx as-is, depths
 * stay consistent. May change this in the future.
 * May make more sense to save b and f separately, and have Tx = -bf dynamically
 * computed. But keep in mind.
 */
void CamInfo::scale(float factor) {
  fx *= factor;
  fy *= factor;
  cx *= factor;
  cy *= factor;
  width *= factor;
  height *= factor;
}

} /* roscpp_utils */
