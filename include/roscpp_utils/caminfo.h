#ifndef CAMINFO_H_DO2NKBWN
#define CAMINFO_H_DO2NKBWN

#include <memory>
#include <Eigen/Core>

#include <sensor_msgs/CameraInfo.h>

namespace roscpp_utils
{

namespace sm = sensor_msgs;

class CamInfo {
 public:
  using Ptr = std::shared_ptr<CamInfo>;

  CamInfo() { }

  void init(float fx,
            float fy,
            float cx,
            float cy,
            float Tx,
            int width,
            int height);

  void init_from_msg(const sm::CameraInfo& caminfo);

  // see note regarding scale of Tx in caminfo.cpp.
  void scale(float factor);

  bool initialized() const { return initialized_; }

  float disp_to_depth(float disp) const {
    return -Tx/disp;
  }

  Eigen::Vector3f uvd_to_xyz(float u, float v, float disp) const {
    float z = -Tx/disp;
    float x = ((u - cx)*z)/fx;
    float y = ((v - cy)*z)/fy;
    return Eigen::Vector3f(x, y, z);
  }

  Eigen::Vector2f xyz_to_uv(float x, float y, float z) const {
    float u = (x*fx)/z + cx;
    float v = (y*fy)/z + cy;
    return Eigen::Vector2f(u, v);
  }

  Eigen::Vector3f lift_projective(float u, float v) const {
    float up = u * fx + cx;
    float vp = v * fy + cy;
    return Eigen::Vector3f(up, vp, 1.);
  }

 public:

  float fx = 0;
  float fy = 0;
  float cx = 0;
  float cy = 0;
  float Tx = 0;

  int width = 0;
  int height = 0;

 private:
  bool initialized_ = false;
};

} /* roscpp_utils */

#endif /* end of include guard: CAMINFO_H_DO2NKBWN */
