#ifndef CV_ENCODING_TAGS_H_OTX5CS3E
#define CV_ENCODING_TAGS_H_OTX5CS3E

#include <opencv2/core/core.hpp>

namespace roscpp_utils
{

template<class T>
struct EncodingString {};

template<>
struct EncodingString<float> {
  const char * encoding() { return "32FC1"; }
};

template<>
struct EncodingString<uint8_t> {
  const char * encoding() { return "8UC1"; }
};

template<>
struct EncodingString<cv::Vec3f> {
  const char * encoding() { return "32FC3"; }
};

template<>
struct EncodingString<cv::Vec3b> {
  const char * encoding() { return "8UC3"; }
};


} /* roscpp_utils */

#endif /* end of include guard: CV_ENCODING_TAGS_H_OTX5CS3E */
