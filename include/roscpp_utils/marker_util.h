#ifndef MARKER_UTIL_H_EIYMT49C
#define MARKER_UTIL_H_EIYMT49C


#include <ros/ros.h>
#include <std_msgs/ColorRGBA.h>
#include <visualization_msgs/Marker.h>

#include <Eigen/Core>
#include <Eigen/Geometry>
#include <Eigen/StdVector>

#include "roscpp_utils/caminfo.h"

namespace roscpp_utils
{

namespace vm = visualization_msgs;

class ColorWrapper {
public:
  ColorWrapper() {
    color_.r = 1.0;
    color_.g = 1.0;
    color_.b = 1.0;
    color_.a = 1.0;
  }

  static ColorWrapper create_from_int(int r, int g, int b) {
    ColorWrapper wrapper;
    wrapper.color_.r = static_cast<float>(r)/255.;
    wrapper.color_.g = static_cast<float>(g)/255.;
    wrapper.color_.b = static_cast<float>(b)/255.;
    return wrapper;
  }

  static ColorWrapper create_from_float(float r, float g, float b) {
    ColorWrapper wrapper;
    wrapper.color_.r = r;
    wrapper.color_.g = g;
    wrapper.color_.b = b;
    return wrapper;
  }

  static ColorWrapper create_from_hexint(int hexint) {
    int r = (0xff0000 & hexint) >> 16;
    int g = (0x00ff00 & hexint) >> 8;
    int b = (0x0000ff & hexint);
    ColorWrapper wrapper;
    wrapper.color_.r = static_cast<float>(r)/255.;
    wrapper.color_.g = static_cast<float>(g)/255.;
    wrapper.color_.b = static_cast<float>(b)/255.;
    return wrapper;
  }

  ColorWrapper& set_alpha(float alpha) {
    color_.a = alpha;
    return *this;
  }

  std_msgs::ColorRGBA get_msg() {
    return color_;
  }

  //virtual ~ColorWrapper();
  //ColorWrapper(const ColorWrapper& other) = delete;
  //ColorWrapper& operator=(const ColorWrapper& other) = delete;
private:
  std_msgs::ColorRGBA color_;

};

class MarkerWrapper {
public:
  MarkerWrapper() {
    this->reset();
  }

  void reset() {
    marker_.header.frame_id = "/world_view";
    //marker.header.stamp.fromSec(ts);
    marker_.header.stamp = ros::Time::now();
    marker_.ns = "semmap";
    marker_.id = 0;
    marker_.type = vm::Marker::TRIANGLE_LIST;
    marker_.action = vm::Marker::ADD;
    marker_.pose.position.x = 0.;
    marker_.pose.position.y = 0.;
    marker_.pose.position.z = 0.;
    marker_.pose.orientation.x = 0.;
    marker_.pose.orientation.y = 0.;
    marker_.pose.orientation.z = 0.;
    marker_.pose.orientation.w = 1.;
    marker_.scale.x = 1.0;
    marker_.scale.y = 1.0;
    marker_.scale.z = 1.0;
    marker_.color.r = static_cast<float>(0xff)/255.;
    marker_.color.g = static_cast<float>(0xa5)/255.;
    marker_.color.b = static_cast<float>(0x00)/255.;
    marker_.color.a = 1.0;
    marker_.lifetime = ros::Duration();
    marker_.points.clear();
    marker_.colors.clear();
  }

  static MarkerWrapper create() {
    return MarkerWrapper();
  }

  static MarkerWrapper create_upside_down_pyramids(float w, float h,
                                                   const std::vector<Eigen::Vector3f>& locations);

  static MarkerWrapper create_frustum2d(const Eigen::Isometry3f& M,
                                        float fx, float cx,
                                        int width,
                                        float max_z);

  static MarkerWrapper create_frustum3d(const Eigen::Isometry3f& M,
                                        float fx, float fy,
                                        float cx, float cy,
                                        int width, int height, float max_z);

  static MarkerWrapper create_frustum3d(const Eigen::Isometry3f& M,
                                        const CamInfo& caminfo,
                                        float max_z) {
    return MarkerWrapper::create_frustum3d(M, caminfo.fx, caminfo.fy,
                                           caminfo.cx, caminfo.cy,
                                           caminfo.width, caminfo.height,
                                           max_z);
  }

  MarkerWrapper& set_namespace(const std::string& ns) {
    marker_.ns = ns; return *this;
  }

  MarkerWrapper& set_id(int id) {
    marker_.id = id; return *this;
  }

  MarkerWrapper& set_frame_id(const std::string& frame_id) {
    marker_.header.frame_id = frame_id; return *this;
  }

  MarkerWrapper& set_stamp(const ros::Time& ts) {
    marker_.header.stamp = ts; return *this;
  }

  MarkerWrapper& set_type_arrow() {
    marker_.type = vm::Marker::ARROW; return *this;
  }

  MarkerWrapper& set_type_cube() {
    marker_.type = vm::Marker::CUBE; return *this;
  }

  MarkerWrapper& set_type_triangle_list() {
    marker_.type = vm::Marker::TRIANGLE_LIST; return *this;
  }

  MarkerWrapper& set_type_line_strip() {
    marker_.type = vm::Marker::LINE_STRIP; return *this;
  }

  MarkerWrapper& set_type_line_list() {
    marker_.type = vm::Marker::LINE_LIST; return *this;
  }

  MarkerWrapper& set_type_sphere() {
    marker_.type = vm::Marker::SPHERE; return *this;
  }

  MarkerWrapper& set_type_cube_list() {
    marker_.type = vm::Marker::CUBE_LIST; return *this;
  }

  MarkerWrapper& set_type_sphere_list() {
    marker_.type = vm::Marker::SPHERE_LIST; return *this;
  }

  MarkerWrapper& set_type_cylinder() {
    marker_.type = vm::Marker::CYLINDER; return *this;
  }

  MarkerWrapper& set_type_text() {
    marker_.type = vm::Marker::TEXT_VIEW_FACING; return *this;
  }

  MarkerWrapper& set_type_points() {
    marker_.type = vm::Marker::POINTS; return *this;
  }

  MarkerWrapper& set_action_add() {
    marker_.action = vm::Marker::ADD; return *this;
  }

  MarkerWrapper& set_action_delete() {
    marker_.action = vm::Marker::DELETE; return *this;
  }

  MarkerWrapper& set_action_modify() {
    marker_.action = vm::Marker::MODIFY; return *this;
  }

  MarkerWrapper& set_position(float x, float y, float z) {
    marker_.pose.position.x = x;
    marker_.pose.position.y = y;
    marker_.pose.position.z = z;
    return *this;
  }

  MarkerWrapper& set_position(Eigen::Vector3f& xyz) {
    marker_.pose.position.x = xyz.x();
    marker_.pose.position.y = xyz.y();
    marker_.pose.position.z = xyz.z();
    return *this;
  }

  MarkerWrapper& set_orientation(Eigen::Quaternionf& q) {
    marker_.pose.orientation.x = q.x();
    marker_.pose.orientation.y = q.y();
    marker_.pose.orientation.z = q.z();
    marker_.pose.orientation.w = q.w();
    return *this;
  }

  MarkerWrapper& set_scale(float x, float y, float z) {
    marker_.scale.x = x;
    marker_.scale.y = y;
    marker_.scale.z = z;
    return *this;
  }

  MarkerWrapper& set_rgb_float(float r, float g, float b) {
    marker_.color.r = r;
    marker_.color.g = g;
    marker_.color.b = b;
    return *this;
  }

  MarkerWrapper& set_rgb_int(int r, int g, int b) {
    marker_.color.r = static_cast<float>(r)/255.;
    marker_.color.g = static_cast<float>(g)/255.;
    marker_.color.b = static_cast<float>(b)/255.;
    return *this;
  }

  MarkerWrapper& set_rgb_hexint(int hexint) {
    int r = (0xff0000 & hexint) >> 16;
    int g = (0x00ff00 & hexint) >> 8;
    int b = (0x0000ff & hexint);
    marker_.color.r = static_cast<float>(r)/255.;
    marker_.color.g = static_cast<float>(g)/255.;
    marker_.color.b = static_cast<float>(b)/255.;
    return *this;
  }

  MarkerWrapper& set_color(const std_msgs::ColorRGBA& color) {
    marker_.color = color;
    return *this;
  }

  MarkerWrapper& add_color(const std_msgs::ColorRGBA& color) {
    marker_.colors.push_back(color);
    return *this;
  }

  MarkerWrapper& set_alpha(float alpha) {
    marker_.color.a = alpha;
    return *this;
  }

  MarkerWrapper& set_lifetime(const ros::Duration& lt) {
    marker_.lifetime = lt;
    return *this;
  }

  MarkerWrapper& add_point(const Eigen::Vector3f& xyz) {
    geometry_msgs::Point pt;
    pt.x = xyz.x();
    pt.y = xyz.y();
    pt.z = xyz.z();
    marker_.points.push_back(pt);
    return *this;
  }

  MarkerWrapper& reserve_points(size_t n) {
    marker_.points.reserve(n);
    return *this;
  }

  const vm::Marker& get_msg() {
    return marker_;
  }

  vm::Marker::Ptr get_msg_ptr() {
    return vm::Marker::Ptr(new vm::Marker(marker_));
  }

  //virtual ~MarkerWrapper() { }
  //MarkerWrapper(const MarkerWrapper& other) = delete;
  //MarkerWrapper& operator=(const MarkerWrapper& other) = delete;

private:
  vm::Marker marker_;
};

} /* roscpp_utils { */

#endif /* end of include guard: MARKER_UTIL_H_EIYMT49C */
