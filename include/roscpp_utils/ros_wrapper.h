#ifndef ROS_WRAPPER_H_S0G7QUIW
#define ROS_WRAPPER_H_S0G7QUIW

#include <boost/variant.hpp>
#include <memory>
#include <map>

#include <ros/ros.h>
#include <ros/console.h>

#include <sensor_msgs/CameraInfo.h>

#include <pcl_conversions/pcl_conversions.h>
#include <pcl_ros/point_cloud.h>
#include <cv_bridge/cv_bridge.h>
#include <sensor_msgs/Image.h>

#include <tf/tf.h>
#include <tf/transform_listener.h>
#include <tf_conversions/tf_eigen.h>

#include "roscpp_utils/caminfo.h"
#include "roscpp_utils/cv_encoding_tags.h"

namespace roscpp_utils
{

class RosWrapper {
 public:
  using Ptr = std::shared_ptr<RosWrapper>;

  using RosParam = boost::variant<bool, int, double, std::string>;
  using RosParamMap = std::map<std::string, RosParam>;
  using CamInfoMap = std::map<std::string, CamInfo>;
  using PublisherMap = std::map<std::string, ros::Publisher>;

  RosWrapper() :
      nh_("~")
  {
  }

  virtual ~RosWrapper() { }

  RosWrapper(const RosWrapper& other) = delete;
  RosWrapper& operator=(const RosWrapper& other) = delete;

  /**
   * get ros param, will cache value so only use for immutable param
   */
  template<typename T>
  T get_param(const std::string& name) {
    RosParamMap::iterator itr = parameters_.find(name);
    if (itr != parameters_.end()) {
      RosParam p(itr->second);
      return boost::get<T>(p);
    }
    T pval = nh_.param<T>(name, T());
    RosParam p(pval);
    parameters_[name] = p;
    return pval;
  }

  template<typename T>
  bool has_param(const std::string& name) const {
    RosParamMap::iterator itr = parameters_.find(name);
    if (itr != parameters_.end()) {
      return true;
    }
    return nh_.hasParam(name);
  }

  template<typename M, typename T>
  void subscribe(const std::string& topic, uint32_t qsize, void(T::*fp)(M), T* obj) {
    ros::Subscriber sub(nh_.subscribe(topic, qsize, fp, obj));
    subscribers_.insert(sub);
  }

  template<typename M, typename T>
  void subscribe(const std::string& topic, uint32_t qsize, void(T::*fp)(const boost::shared_ptr<M const>&), T* obj) {
    ros::Subscriber sub(nh_.subscribe(topic, qsize, fp, obj));
    subscribers_.insert(sub);
  }

  /**
   * subscribe to camera info, will only get one caminfo and cache it
   */
  void subscribe_caminfo(const std::string& topic) {
    caminfo_subscribers_[topic] = nh_.subscribe<sensor_msgs::CameraInfo>(topic,
                                                                         1,
                                                                         boost::bind(&RosWrapper::cb_caminfo,
                                                                                     this,
                                                                                     topic, _1));
  }

  /**
   * advertise and keep publisher, use topic to publish later
   */
  template<typename M>
  void advertise(const std::string& topic, uint32_t qsize) {
    publishers_[topic] = nh_.advertise<M>(topic, qsize);
  }

  /**
   * advertise and return publisher
   */
  template<typename M>
  ros::Publisher get_publisher(const std::string& topic, uint32_t qsize) {
    return nh_.advertise<M>(topic, qsize);
  }

  bool has_subscribers(const std::string& topic) const {
    PublisherMap::const_iterator itr = publishers_.find(topic);
    if (itr == publishers_.end()) {
      return false;
    }
    return (itr->second.getNumSubscribers() > 0);
  }

  /**
   * publish, remember to advertise first
   */
  template<typename M>
  void publish(const std::string& topic, const M& msg) {
    PublisherMap::iterator itr = publishers_.find(topic);
    if (itr != publishers_.end()) {
      itr->second.publish(msg);
    }
    // TODO else what?
  }

  template<typename M>
  void publish(const std::string& topic, const boost::shared_ptr<M const>& msg) {
    PublisherMap::iterator itr = publishers_.find(topic);
    if (itr != publishers_.end()) {
      itr->second.publish(msg);
    }
    // TODO else what?
  }

  void publish_pcxyz(const std::string& topic,
                     const PCXYZ::Ptr pc,
                     const ros::Time& ts,
                     const std::string& frame_id) {
    pc->header.frame_id = frame_id;
    pcl_conversions::toPCL(ts, pc->header.stamp);
    PublisherMap::iterator itr = publishers_.find(topic);
    if (itr != publishers_.end()) {
      itr->second.publish(pc);
    }
  }

  void publish_cv(const std::string& topic,
                  const cv::Mat& img,
                  const std::string& encoding,
                  const ros::Time& ts,
                  const std::string& frame_id) {
    bridge_.image = img;
    sensor_msgs::ImagePtr msg(bridge_.toImageMsg());
    msg->header.stamp = ts;
    msg->header.frame_id = frame_id;
    msg->encoding = encoding;
    PublisherMap::iterator itr = publishers_.find(topic);
    if (itr != publishers_.end()) {
      itr->second.publish(msg);
    }
  }

  template<class T>
  void publish_cvT(const std::string& topic,
                   const cv::Mat_<T>& img,
                   const ros::Time& ts,
                   const std::string& frame_id) {
    std::string encoding(EncodingString<T>().encoding());
    this->publish_cv(topic,
                    img,
                    encoding,
                    ts,
                    frame_id);
  }

  bool has_caminfo(const std::string& topic) const {
    CamInfoMap::const_iterator itr = caminfos_.find(topic);
    return (itr != caminfos_.end());
  }

  bool get_caminfo(const std::string& topic, CamInfo& caminfo) const {
    CamInfoMap::const_iterator itr = caminfos_.find(topic);
    if (itr == caminfos_.end()) {
      ROS_INFO_STREAM("no caminfo " << topic << " yet");
      return false;
    }
    caminfo = itr->second;
    return true;
  }

  void set_caminfo(const std::string& topic, const CamInfo& caminfo) {
    caminfos_[topic] = caminfo;
  }

  bool get_tf_matrix(const std::string& parent,
                     const std::string& child,
                     const ros::Time& ts,
                     Eigen::Isometry3d& M) const {
    try {
      tf::StampedTransform rostf;
      tf_listener_.lookupTransform(parent, child, ts, rostf);
      tf::transformTFToEigen(rostf, M);
    } catch (tf::TransformException ex) {
      ROS_INFO_STREAM("no " << parent << "<-" << child << " tf yet");
      return false;
    }
    return true;
  }

  bool get_tf_matrix_float(const std::string& parent,
                           const std::string& child,
                           const ros::Time& ts,
                           Eigen::Isometry3f& M) const {
    Eigen::Isometry3d Md;
    if (!this->get_tf_matrix(parent, child, ts, Md)) { return false; }
    M = Md.cast<float>();
    return true;
  }

 private:
  // TODO see ros throttle implementation
  // on deleting subscribers
  void cb_caminfo(const std::string& topic, const sensor_msgs::CameraInfoConstPtr msg) {
    CamInfoMap::iterator itr = caminfos_.find(topic);
    if (itr == caminfos_.end()) {
      ROS_INFO_STREAM("initializing caminfo " << topic);
      CamInfo caminfo;
      caminfo.init_from_msg(*msg);
      caminfos_[topic] = caminfo;
      caminfo_subscribers_[topic].shutdown();
    }
  }

 private:
  ros::NodeHandle nh_;

  PublisherMap publishers_;
  std::set<ros::Subscriber> subscribers_;

  RosParamMap parameters_;

  CamInfoMap caminfos_;
  std::map<std::string, ros::Subscriber> caminfo_subscribers_;

  cv_bridge::CvImage bridge_;
  tf::TransformListener tf_listener_;
};

} /*  mavs6sensorproc */


} /* roscpp_utils */

#endif /* end of include guard: ROS_WRAPPER_H_S0G7QUIW */
